<?php

namespace Drupal\oauth_client;

use ArrayObject;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\oauth_client\Annotation\ProviderType as ClientTypeAnnotation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ClientTypeManager.
 *
 * @package Drupal\oauth_client
 */
class ProviderTypeManager extends DefaultPluginManager implements ProviderTypeManagerInterface, ContainerInjectionInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * ClientTypeManager constructor.
   * @param \ArrayObject $container_namespaces
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   */
  public function __construct(ArrayObject $container_namespaces, ModuleHandler $module_handler, EntityTypeManager $entity_type_manager, CacheBackendInterface $cache_backend) {
    parent::__construct('Plugin/OAuth/ClientType', $container_namespaces, $module_handler, ProviderTypeInterface::class, ClientTypeAnnotation::class);
    $this->entityTypeManager = $entity_type_manager;
    $this->alterInfo('oauth_client_type');
    $this->setCacheBackend($cache_backend, 'client_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('container.namespaces'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('cache.oauth')
    );
  }


  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|NULL $container
   *   The container to use to search for this service.
   *   The Drupal container will be used if none is provided.
   * @return static
   */
  public static function getService(ContainerInterface $container = NULL) {
    $container = $container ?: \Drupal::getContainer();
    return $container->get('oauth.provider_type.manager');
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    $instance = FALSE;
    if (isset($options['id']) && is_string($options['id'])) {
      $cache_id = Crypt::hashBase64(Json::encode($options));
      if ($cache = $this->cacheGet($cache_id)) {
        $instance = $cache->data;
      }
      else {
        try {
          $instance = $this->createInstance($options['id'], $options);
          $this->cacheSet($cache_id, $instance, Cache::PERMANENT, ['oauth_client_types']);
        }
        catch (PluginException $e) {
          // Nothing needs to be done here, FALSE will be returned.
        }
      }
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($provider_type_id, array $provider_options = array()) {
    return parent::createInstance($provider_type_id, $provider_options);
  }

}
