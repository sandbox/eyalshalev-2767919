<?php


namespace Drupal\oauth_client;


use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

class ClientListBuilder extends ConfigEntityListBuilder {

  public function load() {
    $headers = $this->buildHeader();

    $query = $this->getStorage()->getQuery();
    $query->pager();
    $query->tableSort($headers);

    $entities = $this->getStorage()
      ->loadMultipleOverrideFree($query->execute());


    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return array_merge(
      [
      $this->entityType->getKey('id') => [
        'data' => $this->t('ID'),
        'field' => $this->entityType->getKey('id'),
        'specifier' => $this->entityType->getKey('id'),
        'class' => ['priority-medium'],
      ],
      $this->entityType->getKey('label') => [
        'data' => $this->t('Name'),
        'field' => $this->entityType->getKey('label'),
        'specifier' => $this->entityType->getKey('label'),
      ],
      'provider' => [
        'data' => $this->t('Provider'),
      ]
      ], parent::buildHeader()
    );
  }

  /**
   * @return \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  public function getStorage() {
    return parent::getStorage();
  }

  /**
   * {@inheritdoc}
   * @param \Drupal\oauth_client\ClientInterface $entity
   */
  public function buildRow(EntityInterface $entity) {
    return [
      $this->entityType->getKey('id') => [
        'data' => $entity->id(),
        'class' => ['priority-medium'],
      ],
      $this->entityType->getKey('label') => $entity->label(),
      'provider' => [
        'data' => [
          '#type' => 'details',
          '#title' => $entity->getProviderTypeId(),
          'summary' => $entity->getProviderType()->getSummary(),
        ],
      ],
    ] + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity) {
    return array_map(
      function (array $operation) {
        $operation['url']->setRouteParameter(
          'destination', Url::fromRoute('<current>')
          ->toString()
        );
        return $operation;
      }, parent::getOperations($entity)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t(
      'There are no %label yet', [
        '%label' => $this->entityType->getPluralLabel(),
      ]
    );
    return $build;
  }


}