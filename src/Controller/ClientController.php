<?php


namespace Drupal\oauth_client\Controller;


use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\oauth_client\ClientInterface;
use Drupal\oauth_client\ProviderTypeManager;
use Drupal\oauth_client\ProviderTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ClientController extends EntityController {

  /**
   * @var \Drupal\oauth_client\ProviderTypeManagerInterface
   */
  protected $clientTypeManager;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  public function __construct(ProviderTypeManagerInterface $client_type_manager, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityRepositoryInterface $entity_repository, RendererInterface $renderer, TranslationInterface $string_translation, UrlGeneratorInterface $url_generator, Request $current_request) {
    parent::__construct($entity_type_manager, $entity_type_bundle_info, $entity_repository, $renderer, $string_translation, $url_generator);
    $this->clientTypeManager = $client_type_manager;
    $this->currentRequest = $current_request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      ProviderTypeManager::getService($container),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity.repository'),
      $container->get('renderer'),
      $container->get('string_translation'),
      $container->get('url_generator'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Creates a redirect response to the oauth server to get an authorization
   * code.
   *
   * We wrap our response inside the render context because a cache context is
   * added to the renderer when the return url is converted to string.
   *
   * @param \Drupal\oauth_client\ClientInterface $oauth_client
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *
   * @link https://www.drupal.org/node/2513810
   */
  public function getCode(ClientInterface $oauth_client) {
    $context = new RenderContext();
    $response = $this->renderer->executeInRenderContext(
      $context, function () use ($oauth_client) {
      if ($destination = $this->currentRequest->get('destination')) {
        $this->currentRequest->query->remove('destination');
        $state = $oauth_client->getStateData();
        $state['destination'] = $destination;
        $oauth_client->setStateData($state);
      }

      $redirect_url = $oauth_client->getAuthorizationUrl();

      return new TrustedRedirectResponse($redirect_url->toString());
    }
    );

    if (!$context->isEmpty()) {
      $metadata = $context->pop();
      $response->addCacheableDependency($metadata);
    }

    return $response;
  }

  /**
   * This endpoint should only be used when returning from the oauth server
   * with a valid authentication code.
   *
   * The received code will then be added to the state data of the client entity.
   *
   * @param \Drupal\oauth_client\ClientInterface $oauth_client
   * @param $state
   *   The state string that we sent to OAuth server.
   * @param $code
   *   The authentication code created by the OAuth server.
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *
   * @see \Drupal\oauth_client\ClientInterface::isValidStateKey()
   * @see \Drupal\oauth_client\ClientInterface::getStateData()
   */
  public function authorizeCode(ClientInterface $oauth_client, $state, $code) {
    // Only process valid requests.
    if ($oauth_client->isValidStateKey($state)) {

      // Adds the code to the state data to be accessible by other components.
      $state_data = $oauth_client->getStateData();
      $state_data['code'] = $code;
      $oauth_client->setStateData($state_data);

      if (isset($state_data['destination'])) {
        $destination = $state_data['destination'];
      }
      else {
        $destination = Url::fromRoute('<front>')->toString();
      }

      return new RedirectResponse($destination);
    }
    else {
      // @TODO replace this with a more graceful solution.
      throw new \LogicException('The received state key is not valid');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addPage($entity_type_id) {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if ($entity_type->isSubclassOf(ClientInterface::class)) {
      $client_type_definitions = $this->clientTypeManager->getDefinitions();
      switch (count($client_type_definitions)) {
        case 0:
          return [
            '#type' => 'inline_template',
            '#template' => '{% trans %}There are no OAuth client type plugins available.<br>Please see the{% endtrans %} {{ doc }}.',
            '#context' => [
              'doc' => Link::fromTextAndUrl('documentation', Url::fromUri('https://drupal.org/project/oauth_client'))
                ->toRenderable(),
            ],
          ];
        case 1:
          $client_type_definition = reset($client_type_definitions);
          return $this->redirect(
            'entity.oauth_client.add_form', [
              'client_type' => $client_type_definition['id'],
            ]
          );
        default:
          return [
            'list' => [
              '#theme' => 'item_list',
              '#title' => $this->t('Choose what type of client you want to create'),
              '#items' => array_map(
                function (array $client_type_definition) use ($entity_type) {
                  $text = $this->t(
                    'Create a new %type @label', [
                      '%type' => $client_type_definition['label'],
                      '@label' => $entity_type->getLowercaseLabel(),
                    ]
                  );
                  return Link::createFromRoute(
                    $text, 'entity.oauth_client.add_form', [
                      'client_type' => $client_type_definition['id'],
                    ]
                  )->toRenderable();
                }, $client_type_definitions
              ),
            ],
          ];
      }

    }
    else {
      throw new \BadMethodCallException("The received entity type ({$entity_type_id}) does not implement the ClientInterface");
    }
  }

}