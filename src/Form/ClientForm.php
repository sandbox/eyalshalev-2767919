<?php

namespace Drupal\oauth_client\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form class for the oauth_client entity.
 */
class ClientForm extends EntityForm {

  /**
   * The oauth client storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * The oauth client entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * Constructs a new oauth client form.
   */
  public function __construct() {
  }

  public static function create(ContainerInterface $container) {
    return new static();
  }

  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    parent::setEntityTypeManager($entity_type_manager);
    $this->entityType = $this->entityTypeManager->getDefinition('oauth_client');
    $this->storage = $this->entityTypeManager->getStorage('oauth_client');
    $this->storage = $this->entityTypeManager->getStorage('oauth_client');
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form[$this->entityType->getKey('label')] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $this->getEntity()->label(),
    ];

    $form[$this->entityType->getKey('id')] = [
      '#type' => 'machine_name',
      '#default_value' => $this->getEntity()->id(),
      '#disabled' => !$this->getEntity()->isNew(),
      '#maxlength' => 64,
      '#description' => $this->t('A unique name for this item. It must only contain lowercase letters, numbers, and underscores.'),
      '#machine_name' => [
        'exists' => [$this->storage, 'load'],
        'source' => [$this->entityType->getKey('label')],
      ],
    ];

    $form['provider'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $form['provider'] = $this->getEntity()
      ->getProviderType()
      ->buildConfigurationForm($form['provider'], $this->getSubFormState($form_state, 'provider'));

    $form['provider']['type'] = [
      '#type' => 'value',
      '#value' => $this->getEntity()->getProviderTypeId(),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   * @return \Drupal\oauth_client\ClientInterface
   */
  public function getEntity() {
    return parent::getEntity();
  }

  protected function getSubFormState(FormStateInterface $main_form_state, $key) {
    $path = ['sub_form_state', $key];
    $sub_form_state = $main_form_state->get($path);
    if (empty($sub_form_state)) {
      $sub_form_state = new FormState();
      $main_form_state->set($path, $sub_form_state);
    }

    $sub_form_state
      ->setValues($main_form_state->getValue($key, []))
      ->setUserInput(NestedArray::getValue($main_form_state->getUserInput(), [$key]) ?: []);

    if ($main_form_state->getCompleteForm()) {
      $sub_form_state->setCompleteForm($main_form_state->getCompleteForm());
    }

    return $sub_form_state;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $sub_form_state = $this->getSubFormState($form_state, 'provider');
    $this->getEntity()
      ->getProviderType()
      ->validateConfigurationForm($form['provider'], $sub_form_state);
    foreach ($sub_form_state->getErrors() as $error_path => $error) {
      $form_state->setErrorByName('provider][' . $error_path, $error);
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sub_form_state = $this->getSubFormState($form_state, 'provider');
    $this->getEntity()
      ->getProviderType()
      ->submitConfigurationForm($form['provider'], $sub_form_state);
    parent::submitForm($form, $form_state);
    drupal_set_message(
      $this->t(
        '@title has been created successfully', [
        '@title' => $this->getEntity()->label(),
      ]
      )
    );

    if ($this->getEntity()->access('update')) {
      $form_state->setRedirect(
        'entity.oauth_client.edit_form', [
        'oauth_client' => $this->getEntity()->id(),
      ]
      );
    }
    elseif ($this->currentUser()
      ->hasPermission($this->entityType->getAdminPermission())
    ) {
      $form_state->setRedirect('entity.oauth_client.collection');
    }
    else {
      $form_state->setRedirect('<front>');
    }
  }

  public function save(array $form, FormStateInterface $form_state) {
    return parent::save($form, $form_state);
  }

  /**
   * {@inheritDoc}
   * @return \Drupal\oauth_client\ClientInterface
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getRawParameter($entity_type_id) !== NULL) {
      $entity = $route_match->getParameter($entity_type_id);
    }
    else {
      $provider_type = $route_match->getParameter('provider_type');
      $values = [
        'provider' => [
          'type' => $provider_type,
        ],
      ];

      $entity = $this->entityTypeManager->getStorage($entity_type_id)
        ->create($values);
    }

    return $entity;
  }


}