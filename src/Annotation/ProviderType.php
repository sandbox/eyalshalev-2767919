<?php

namespace Drupal\oauth_client\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * @Annotation
 */
class ProviderType extends Plugin {

  /**
   * The client type plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the client type plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * The description of the client type plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;
}