<?php


namespace Drupal\oauth_client;


use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class ClientAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $result = AccessResult::neutral();

    switch ($operation) {
      case 'update':
        $result = AccessResult::allowedIfHasPermission($account, 'edit oauth clients');
        break;
      case 'delete':
        $result = AccessResult::allowedIfHasPermission($account, 'delete oauth clients');
        break;
      case 'authorize':
        $result = AccessResult::allowedIfHasPermission($account, 'authorize oauth clients');
        break;
    }

    return $result->orIf(parent::checkAccess($entity, $operation, $account));
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'create oauth clients')
      ->orIf(parent::checkCreateAccess($account, $context, $entity_bundle));
  }

}