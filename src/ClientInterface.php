<?php

namespace Drupal\oauth_client;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for the oauth client entity.
 */
interface ClientInterface extends ConfigEntityInterface {


  /**
   * @return \Drupal\oauth_client\ProviderTypeInterface
   */
  public function getProviderType();


  /**
   * @return string|null
   */
  public function getProviderTypeId();

  /**
   * @return \League\OAuth2\Client\Provider\AbstractProvider
   */
  public function getProvider();

  /**
   * The authorization url is used to get a code from the oauth server.
   *
   * @return \Drupal\Core\Url
   */
  public function getAuthorizationUrl();

  /**
   * Validates a given string against the unique state key of this client.
   * @param string $key
   * @return bool
   */
  public function isValidStateKey($key);

  /**
   * Gets the data that was saved on this client state storage.
   *
   * @return array
   */
  public function getStateData();

  /**
   * Saves data to the state storage of this client.
   *
   * @param array $data
   */
  public function setStateData(array $data);
}