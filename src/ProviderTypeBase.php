<?php


namespace Drupal\oauth_client;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for OAuth client type plugins.
 */
abstract class ProviderTypeBase extends PluginBase implements ProviderTypeInterface, ContainerFactoryPluginInterface {

  /**
   * @var array
   */
  protected $configuration;

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * @var \League\OAuth2\Client\Provider\AbstractProvider
   */
  protected $provider;

  /**
   * ClientTypeBase constructor.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\Client $http_client
   *   The http client object that will be passed to the oauth client provider.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form += [
      '#type' => 'details',
      '#title' => $this->t('Provider options'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['clientId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#required' => TRUE,
      '#default_value' => $this->getConfigurationValue('clientId'),
    ];

    $secret = $this->getConfigurationValue('clientSecret');
    $form['clientSecret'] = [
      '#type' => 'password',
      '#title' => $this->t('Client secret'),
      '#required' => empty($secret),
      '#description' => $this->t('The secret pass code that is used to authenticate with the OAuth server.'),
      '#placeholder' => $secret ? '*********' : NULL,
      '#default_value' => $secret,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function &getConfigurationValue($key = []) {
    $key = is_array($key) ? $key : [$key];
    return NestedArray::getValue($this->configuration, $key);
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    foreach (['clientId', 'clientSecret'] as $item) {
      $default_value = $this->getConfigurationValue($item);
      $value = $form_state->getValue($item, $default_value);
      $this->setConfigurationValue($item, $value);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function setConfigurationValue($key = [], $value) {
    $key = is_array($key) ? $key : [$key];
    NestedArray::setValue($this->configuration, $key, $value);
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'clientId' => NULL,
      'clientSecret' => NULL,
      'redirectUri' => NULL,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getSummary() {

    $keys = array_keys(array_filter($this->configuration));

    $values = array_map(
      function ($key) {
        $value = $this->configuration[$key];
        if (is_array($value)) {
          return [
            '#theme' => 'item_list',
            '#title' => $key,
            '#items' => $value,
          ];
        }
        else {
          return [
            '#type' => 'item',
            '#title' => $key,
            '#markup' => $value,
          ];
        }
      }, $keys
    );

    return array_combine($keys, $values);
  }

  /**
   * {@inheritDoc}
   */
  public function createProvider(array $options = []) {
    return $this->doCreateProvider(
      NestedArray::mergeDeep($this->getOptions(), $options),
      $this->getCollaborators()
    );
  }

  /**
   * Constructs an instance of the provider object.
   *
   * @param array $options
   * @param array $collaborators
   * @return \League\OAuth2\Client\Provider\AbstractProvider
   */
  abstract protected function doCreateProvider(array $options, array $collaborators);

  /**
   * Constructs the options array that is passed to the provider constructor.
   *
   * @see \Drupal\oauth_client\ClientTypeBase::doCreateProvider()
   * @return mixed
   */
  protected function getOptions() {
    $options = $this->getConfiguration();
    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritDoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * The collaborators array that is passed to the provider object.
   *
   * @return array
   */
  protected function getCollaborators() {
    return [
      'httpClient' => $this->httpClient,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function calculateDependencies() {
    return [
      'module' => ['oauth_client'],
    ];
  }

}