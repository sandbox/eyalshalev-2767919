<?php


namespace Drupal\oauth_client\Plugin\Derivative;


use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\oauth_client\ProviderTypeManager;
use Drupal\oauth_client\ProviderTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientAddLocalAction extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  protected $providerTypeManager;

  /**
   * Constructs an ClientMenuLinkDeriver object.
   *
   * @param \Drupal\oauth_client\ProviderTypeManagerInterface $provider_type_manager
   */
  public function __construct(ProviderTypeManagerInterface $provider_type_manager) {
    $this->providerTypeManager = $provider_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(ProviderTypeManager::getService($container));
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    if (empty($this->derivatives)) {
      $this->derivatives = [];

      foreach ($this->providerTypeManager->getDefinitions() as $definition) {
        $derivative = [
          'route_name' => 'entity.oauth_client.add_form',
          'route_parameters' => [
            'provider_type' => $definition['id'],
            'destination' => Url::fromRoute('<current>')->toString(),
          ],
          'title' => $this->t(
            'Add @type client', [
              '@type' => $definition['label'],
            ]
          ),
          'appears_on' => ['entity.oauth_client.collection'],
        ];
        $this->derivatives['entity.oauth_client.add_form.' . $definition['id']] = $derivative + $base_plugin_definition;
      }
    }
    return $this->derivatives;
  }


}