<?php


namespace Drupal\oauth_client\Plugin\OAuth\ClientType;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\oauth_client\ProviderTypeBase;
use League\OAuth2\Client\Provider\GenericProvider;

/**
 * @ProviderType(
 *   id = "generic",
 *   label = @Translation("Generic provider")
 * )
 */
class GenericProviderType extends ProviderTypeBase {

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['urlAuthorize'] = [
      '#type' => 'url',
      '#title' => $this->t('Authorization url'),
      '#description' => $this->t('This url is used to an access code.'),
      '#required' => TRUE,
    ];

    $form['urlAccessToken'] = [
      '#type' => 'url',
      '#title' => $this->t('Access token url'),
      '#description' => $this->t('This url is trades a valid access code with an access token.'),
      '#required' => TRUE,
    ];

    $form['urlResourceOwnerDetails'] = [
      '#type' => 'url',
      '#title' => $this->t('Resource owner details url'),
      '#description' => $this->t('This url is used to obtain the details of the resource owner.'),
      '#required' => TRUE,
    ];

    $form['accessTokenMethod'] = [
      '#type' => 'select',
      '#title' => $this->t('Access token method'),
      '#options' => [
        'POST' => 'POST',
        'GET' => 'GET',
      ],
      '#description' => $this->t('The method used to fetch access tokens.'),
    ];

    $form['accessTokenResourceOwnerId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Resource owner identifier (access token request)'),
      '#description' => $this->t('The key used in the access token response to identify the resource owner ID.'),
    ];

    $form['scopeSeparator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scope separator'),
      '#description' => $this->t('The string used when separating scopes in the access token url.'),
    ];

    $form['responseError'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Response error key'),
      '#description' => $this->t('The key used for error messages in the response object.'),
    ];

    $form['responseCode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Response error code'),
      '#description' => $this->t('The error code to use when a response error should be thrown.'),
    ];

    $form['responseResourceOwnerId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Resource owner identifier (details request)'),
      '#description' => $this->t('The key used in the resource owner details response to identify the resource owner ID.'),
    ];

    $form['scopes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('The requested scopes'),
      '#description' => $this->t('Scopes are separated by a new line..'),
    ];


    $config_keys = [
      'urlAuthorize',
      'urlAccessToken',
      'urlResourceOwnerDetails',
      'accessTokenMethod',
      'accessTokenResourceOwnerId',
      'scopeSeparator',
      'responseError',
      'responseCode',
      'responseResourceOwnerId',
    ];

    foreach ($config_keys as $config_key) {
      $default_value = $this->getConfigurationValue([$config_key]);
      if ($config_key === 'scopes' && is_array($default_value)) {
        $default_value = implode("\n", $default_value);
      }
      $form[$config_key]['#default_value'] = $default_value;
    }

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $scopes = $form_state->getValue(['scopes']);
    $separator = $form_state->getValue(['scopeSeparator']);
    if (!empty($scopes) && !empty($separator) && strpos($scopes, $separator)) {
      $form_state->setErrorByName('scopes', 'The separator string cannot appear inside the scopes text.');
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $config_keys = [
      'urlAuthorize',
      'urlAccessToken',
      'urlResourceOwnerDetails',
      'accessTokenMethod',
      'accessTokenResourceOwnerId',
      'scopeSeparator',
      'responseError',
      'responseCode',
      'responseResourceOwnerId',
    ];

    foreach ($config_keys as $config_key) {
      $default_value = $this->getConfigurationValue([$config_key]);
      $value = $form_state->getValue([$config_key], $default_value);
      $this->setConfigurationValue([$config_key], $value);
    }

    if ($scopes = $form_state->getValue(['scopes'])) {
      $this->setConfigurationValue(['scopes'], explode("\n", $scopes));
    }
    else {
      $this->setConfigurationValue(['scopes'], []);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return NestedArray::mergeDeep(
      [
        'urlAuthorize' => NULL,
        'urlAccessToken' => NULL,
        'urlResourceOwnerDetails' => NULL,
        'accessTokenMethod' => NULL,
        'accessTokenResourceOwnerId' => NULL,
        'scopeSeparator' => NULL,
        'responseError' => NULL,
        'responseCode' => NULL,
        'responseResourceOwnerId' => NULL,
        'scopes' => NULL,
      ], parent::defaultConfiguration()
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function doCreateProvider(array $options, array $collaborators) {
    return new GenericProvider($options, $collaborators);
  }

}