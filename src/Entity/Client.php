<?php

namespace Drupal\oauth_client\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\oauth_client\ClientInterface;
use Drupal\oauth_client\ProviderTypeManager;

/**
 * @ConfigEntityType(
 *   id = "oauth_client",
 *   label = @Translation("OAuth client", context = "Entity type label"),
 *   label_plural = @Translation("OAuth clients", context = "Entity type label"),
 *   label_count = @PluralTranslation(
 *     singular = "@count oauth client",
 *     plural = "@count oauth clients",
 *     context = "Entity type label"
 *   ),
 *   group = "web_services",
 *   group_label = @Translation("Web services", context = "Entity type group"),
 *   handlers = {
 *     "access" = "Drupal\oauth_client\ClientAccessControlHandler",
 *     "list_builder" = "Drupal\oauth_client\ClientListBuilder",
 *     "form" = {
 *       "default" = "Drupal\oauth_client\Form\ClientForm",
 *       "add" = "Drupal\oauth_client\Form\ClientForm",
 *       "edit" = "Drupal\oauth_client\Form\ClientForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\oauth_client\Entity\ClientRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer oauth clients",
 *   config_prefix = "client",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name"
 *   },
 *   links = {
 *     "collection" = "/admin/config/oauth/client",
 *     "add-page" = "/admin/config/oauth/client/add",
 *     "add-form" = "/admin/config/oauth/client/add/{provider_type}",
 *     "edit-form" = "/admin/config/oauth/client/{oauth_client}",
 *     "delete-form" = "/admin/config/oauth/client/{oauth_client}/delete",
 *     "get-code" = "/admin/config/oauth/client/{oauth_client}/get-code",
 *     "authorize-code" = "/admin/config/oauth/client/{oauth_client}/auth-code/{state}/{code}"
 *   }
 * )
 */
class Client extends ConfigEntityBase implements ClientInterface, EntityWithPluginCollectionInterface {

  /**
   * @var string
   */
  protected $id;

  /**
   * @var array
   */
  protected $provider_options;

  /**
   * @var \League\OAuth2\Client\Provider\AbstractProvider
   */
  protected $provider;

  /**
   * @var string
   */
  protected $name;

  /**
   * @var \Drupal\Component\Plugin\LazyPluginCollection
   */
  protected $providerTypePluginCollection;

  /**
   * @var string
   */
  protected $stateKey;

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $stateStorage;

  /**
   * {@inheritDoc}
   */
  public function getAuthorizationUrl() {
    $base_uri = $this->getProviderType()->createProvider()
      ->getAuthorizationUrl(['state' => $this->generateStateKey()]);

    return Url::fromUri($base_uri)->setAbsolute(TRUE);
  }

  /**
   * {@inheritDoc}
   */
  public function getProviderType() {
    return $this
      ->getProviderTypePluginCollection()
      ->get($this->getProviderTypeId());
  }

  /**
   * Initializes (if not cached) the client type plugin [single] collection object.
   *
   * @see \Drupal\oauth_client\Entity\Client::getProviderType()
   * @return \Drupal\Component\Plugin\LazyPluginCollection
   */
  protected function getProviderTypePluginCollection() {
    if (empty($this->providerTypePluginCollection)) {
      $this->providerTypePluginCollection = new DefaultSingleLazyPluginCollection(
        ProviderTypeManager::getService(),
        $this->getProviderTypeId(),
        $this->getProviderTypeConfiguration()
      );
    }
    return $this->providerTypePluginCollection;
  }

  /**
   * {@inheritDoc}
   */
  public function getProviderTypeId() {
    return isset($this->provider_options['type']) ? $this->provider_options['type'] : NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getProvider() {
    
    if (empty($this->provider)) {
      $this->provider = $this->getProviderType()->createProvider([
        'state' => $this->getStateKey()
      ]);
    }

    return $this->provider;
  }

  /**
   * Helping method that is used by getClientTypePluginCollection to initialize
   * the client_type plugin with the client configuration.
   *
   * @see \Drupal\oauth_client\Entity\Client::getProviderTypePluginCollection()
   * @return array
   */
  protected function getProviderTypeConfiguration() {
    $configuration = $this->provider_options;
    if (!$this->isNew()) {
      // Set this entity authorization code url as the redirectUri of the provider.
      $redirect = $this->toUrl('authorize-code')->setAbsolute(TRUE);
      $configuration['redirectUri'] = $redirect->toString();
    }
    return $configuration;
  }

  /**
   * @return string
   */
  protected function getStateKey() {
    if (!empty($this->id()) && empty($this->stateKey)) {
      /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface $session */
      $session = \Drupal::service('session');
      $json = [
        'client_id' => $this->id(),
        'session_id' => $session->getId(),
      ];
      $this->stateKey = Crypt::hmacBase64(Json::encode($json), Settings::getHashSalt());
    }
    return $this->stateKey;
  }

  /**
   * {@inheritDoc}
   */
  public function isValidStateKey($key) {
    return $key === $this->getStateKey();
  }

  /**
   * {@inheritDoc}
   */
  public function getStateData() {
    return $this->getStateStorage()
      ->get($this->getStateKey(), $this->defaultStateData());
  }

  /**
   * @return \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  public function getStateStorage() {
    if (empty($this->stateStorage)) {
      $this->stateStorage = \Drupal::service('oauth.state');
    }
    return $this->stateStorage;
  }

  /**
   * @return array
   */
  protected function defaultStateData() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function setStateData(array $data) {
    $this->getStateStorage()
      ->setWithExpire($this->getStateKey(), $data, $this->getStateExpiration());
  }

  /**
   * The seconds that the oauth.state key-value states will be kept.
   * @return int
   */
  protected function getStateExpiration() {
    return \Drupal::getContainer()->getParameter('oauth.state.expiration');
  }

  /**
   *
   * @return bool|array
   */
  public function getState() {
    $state = FALSE;
    if (is_string($this->stateKey)) {
      $state = $this->getStateStorage()->get($this->stateKey, FALSE);
    }
    return $state;
  }

  /**
   * {@inheritDoc}
   */
  public function getPluginCollections() {
    return [
      'provider' => $this->getProviderTypePluginCollection(),
    ];
  }

}