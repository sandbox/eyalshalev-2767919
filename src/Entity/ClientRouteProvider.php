<?php


namespace Drupal\oauth_client\Entity;


use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\oauth_client\Controller\ClientController;
use Symfony\Component\Routing\Route;

class ClientRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    if ($route = $this->getCollectionRoute($entity_type)) {
      $collection->add('entity.' . $entity_type->id() . '.collection', $route);
    }

    if ($route = $this->getAuthorizeCodeRoute($entity_type)) {
      $collection->add('entity.' . $entity_type->id() . '.authorize_code', $route);
    }

    if ($route = $this->getCodeRoute($entity_type)) {
      $collection->add('entity.' . $entity_type->id() . '.get_code', $route);
    }

    return $collection;
  }

  /**
   * Gets the collection page route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('collection')) {
      /** @var TranslatableMarkup $title */
      $title = $entity_type->getPluralLabel();

      $route = new Route($entity_type->getLinkTemplate('collection'));
      $route->setDefault('_entity_list', $entity_type->id())
        ->setDefault('_title', $title->getUntranslatedString())
        ->setOption('_admin_route', TRUE)
        ->setRequirement('_permission', 'view oauth clients');

      return $route;
    }
    return NULL;
  }

  /**
   * Gets the authorization route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getAuthorizeCodeRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('authorize-code')) {
      $controller = implode('::', [ClientController::class, 'authorizeCode']);
      $route = new Route($entity_type->getLinkTemplate('authorize-code'));
      $route->setDefault('_controller', $controller)
        ->setDefault('_title', 'Authorize code')
        ->setDefault('state', NULL)
        ->setDefault('code', NULL)
        ->setOption(
          'parameters', [
          $entity_type->id() => [
            'type' => 'entity:' . $entity_type->id(),
          ],
        ]
        )
        ->setOption('_admin_route', TRUE)
        ->setRequirement('_entity_access', $entity_type->id() . '.authorize');

      return $route;
    }
    return NULL;
  }

  /**
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCodeRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('get-code')) {
      $controller = implode('::', [ClientController::class, 'getCode']);
      $route = new Route($entity_type->getLinkTemplate('get-code'));
      $route->setDefault('_controller', $controller)
        ->setDefault('_title', 'Get code')
        ->setOption('_admin_route', TRUE)
        ->setOption(
          'parameters', [
          $entity_type->id() => [
            'type' => 'entity:' . $entity_type->id(),
          ],
        ]
        )
        ->setRequirement('_entity_access', $entity_type->id() . '.authorize');

      return $route;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAddPageRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('add-page')) {
      $route = new Route($entity_type->getLinkTemplate('add-page'));
      $route->setDefault('_controller', ClientController::class . '::addPage')
        ->setDefault('_title_callback', ClientController::class . '::addTitle')
        ->setDefault('entity_type_id', $entity_type->id())
        ->setOption('_admin_route', TRUE)
        ->setRequirement('_entity_create_any_access', $entity_type->id());

      return $route;
    }
    return NULL;
  }

}