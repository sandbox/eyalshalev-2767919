<?php

namespace Drupal\oauth_client;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface ClientTypeManagerInterface.
 *
 * @package Drupal\oauth_client
 */
interface ProviderTypeManagerInterface extends PluginManagerInterface {

  /**
   * Gets a preconfigured instance of a provider type plugin.
   *
   * @param array $provider_options
   *   The array of options used to create the instance of the plugin.
   *   The `id` key must exist in the array.
   *
   * @return \Drupal\oauth_client\ProviderTypeInterface|false
   *   A fully configured oauth client type plugin instance.
   *   If no instance can be retrieved, FALSE will be returned.
   */
  public function getInstance(array $provider_options);

  /**
   * Creates a pre-configured instance of the provider type plugin.
   *
   * @param string $provider_type_id
   *   The ID of the plugin being instantiated.
   * @param array $provider_options
   *   The array of options passed to the provider constructor.
   *
   * @return \Drupal\oauth_client\ProviderTypeInterface
   *   A fully configured provider type plugin instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the instance cannot be created, such as if the ID is invalid.
   */
  public function createInstance($provider_type_id, array $provider_options = array());

}
