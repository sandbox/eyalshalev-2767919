<?php

namespace Drupal\oauth_client;

use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * All the oauth client type plugins are required to extend this interface.
 */
interface ProviderTypeInterface extends ConfigurablePluginInterface, PluginFormInterface {

  /**
   * Creates a new instance of a client provider.
   *
   * @param array $options
   *
   * @return \League\OAuth2\Client\Provider\AbstractProvider
   */
  public function createProvider(array $options = []);

  /**
   * Gets a specific configuration value for this plugin instance.
   *
   * @param array|string $key
   * @return mixed
   */
  public function &getConfigurationValue($key = []);

  /**
   * Sets a specific configuration value for this plugin instance.
   *
   * @param array|string $key
   * @param mixed $value
   */
  public function setConfigurationValue($key = [], $value);

  /**
   * Returns a render array of the summary of this plugin instance.
   *
   * @return array
   */
  public function getSummary();
}